<?php
class JConfig {
	public $offline = '0';
	public $offline_message = 'Hier entsteht die neue Webseite der REWO GmbH.<br />';
	public $display_offline_message = '1';
	public $offline_image = '';
	public $sitename = 'Fachhandelspartner für Jablotron Alarmanlagen OASiS,  Optex, Ja-100, Sicherheitstechnik und Industriefunktechnik Deutschland';
	public $editor = 'jce';
	public $captcha = '0';
	public $list_limit = '100';
	public $access = '1';
	public $debug = '0';
	public $debug_lang = '0';
	public $dbtype = 'mysqli';
	public $host = 'localhost';
	public $user = 'd0232e4f';
	public $password = 'Rp3KJeat8QubLWDY';
	public $db = 'd0232e4f';
	public $dbprefix = 'q4k9g_';
	public $live_site = '';
	public $secret = 'dF9H3XiIYLbKdwzp';
	public $gzip = '1';
	public $error_reporting = 'none';
	public $helpurl = 'https://help.joomla.org/proxy?keyref=Help{major}{minor}:{keyref}&lang={langcode}';
	public $ftp_host = '127.0.0.1';
	public $ftp_port = '21';
	public $ftp_user = '';
	public $ftp_pass = '';
	public $ftp_root = '';
	public $ftp_enable = '0';
	public $offset = 'Europe/Berlin';
	public $offset_user = 'UTC';
	public $mailer = 'mail';
	public $mailfrom = 'cg@089webdesign.de';
	public $fromname = 'REWO GmbH';
	public $sendmail = '/usr/sbin/sendmail';
	public $smtpauth = '0';
	public $smtpuser = '';
	public $smtppass = '';
	public $smtphost = 'localhost';
	public $smtpsecure = 'none';
	public $smtpport = '25';
	public $caching = '0';
	public $cache_handler = 'file';
	public $cachetime = '15';
	public $MetaDesc = 'Fachhandelspartner u.a. Jablotron Oasis, Optex, Teleradio für Alarmanlagen, Sicherheitstechnik und Indstriefunktechnik ';
	public $MetaKeys = 'JA-100,Jablotron,Optex,Oasis,Teleradio,Deutschland,Bewegungsmelder,Funkalarm,Einbruchmeldetechnik,Passiv-Infrarot,Aktiv-Infrarot,Lichtschranken,Panasonic,Redwall,Redwatch, Aussensicherungssysteme,Freigeländeüberwachung, Videoüberwachung,Brandmelder,Alarmanlagen,CCTV, Einruchmeldeanlagen, Sicherheitstechnik,Alarmierung, Alarmübertragung, Wählgeräte,TWG,ISDN,Melder, Funkeinbruchmeldesysteme,Zifferncode,Verteiler, Sensoren,Signalgeber,Industriefunk, Funkfernsteuerungen,Türsensoren,';
	public $MetaTitle = '1';
	public $MetaAuthor = '0';
	public $robots = '';
	public $sef = '1';
	public $sef_rewrite = '1';
	public $sef_suffix = '0';
	public $unicodeslugs = '1';
	public $feed_limit = '10';
	public $log_path = '/www/htdocs/w0138d27/rewo_dev/logs';
	public $tmp_path = '/www/htdocs/w0138d27/rewo_dev/tmp';
	public $lifetime = '90';
	public $session_handler = 'database';
	public $MetaRights = '';
	public $sitename_pagetitles = '2';
	public $force_ssl = '0';
	public $feed_email = 'author';
	public $cookie_domain = '';
	public $cookie_path = '';
	public $MetaVersion = '0';
	public $cache_platformprefix = '0';
	public $memcache_persist = '1';
	public $memcache_compress = '0';
	public $memcache_server_host = 'localhost';
	public $memcache_server_port = '11211';
	public $memcached_persist = '1';
	public $memcached_compress = '0';
	public $memcached_server_host = 'localhost';
	public $memcached_server_port = '11211';
	public $redis_persist = '1';
	public $redis_server_host = 'localhost';
	public $redis_server_port = '6379';
	public $redis_server_auth = '';
	public $redis_server_db = '0';
	public $proxy_enable = '0';
	public $proxy_host = '';
	public $proxy_port = '';
	public $proxy_user = '';
	public $proxy_pass = '';
	public $mailonline = '1';
	public $massmailoff = '0';
	public $session_memcache_server_host = 'localhost';
	public $session_memcache_server_port = '11211';
	public $session_memcached_server_host = 'localhost';
	public $session_memcached_server_port = '11211';
	public $frontediting = '0';
	public $asset_id = '1';
	public $replyto = '';
	public $replytoname = '';
	public $session_redis_server_host = 'localhost';
	public $session_redis_server_port = '6379';
	public $shared_session = '0';
	public $session_redis_server_db = '0';
	public $session_redis_persist = '1';
	public $session_redis_server_auth = '';
}