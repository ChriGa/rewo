<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 

defined('_JEXEC') or die;

?>
<?php if ($this->countModules('bottom-last')) : ?>
	<div class="bottom-last fullwidth">
		<div class="bottom-last-wrap innerwidth">
			<div class="row-fluid">
				<?php if (!$detect->isMobile()) : ?>			
					<div class="span12 module_bottom position_bottom-last">
						<jdoc:include type="modules" name="bottom-last" style="xhtml" />
					</div>
				<?php else: ?>
					<div class="span12 module_bottom position_bottom-last">
						<jdoc:include type="modules" name="bottom-last-mobile" style="xhtml" />
					</div>				
				<?php endif; ?>
			</div> 	
		</div> 
	</div>	
<?php endif ?>
	
		
