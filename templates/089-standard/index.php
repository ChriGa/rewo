<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html lang="de">
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>
	<?php 
		$user = JFactory::getUser();
		if(isset($user->name)) { $RegUser = true; } else {$RegUser = false; };
	?>

<body id="bodySite" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class
	. print (!$detect->isMobile()) ? " desktop " : " mobile $detectAgent";
		print ($RegUser) ? " regUser" : " guest";
?>">
	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<?php			
				
			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
			
			// including slider
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');			

			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');				
					
			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	
			
			// including top2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top2.php');	
									
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	

			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including bottom2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');

			// including bottom-last
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom3.php');			
			
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

<script type="text/javascript">

	jQuery(document).ready(function() {

		jQuery(window).on('load', ()=> {
			jQuery('body').addClass('rdy');
		});
		
<?php if ($detect->isMobile()) : ?> //mobile

		jQuery.extend(jQuery.lazyLoadXT, { // lazyLoad
			  edgeY:  100,
			  srcAttr: 'data-src'
			});		
		<?php else : ?> //desktop
			jQuery.extend(jQuery.lazyLoadXT, {
				  edgeY:  200,
			  	srcAttr: 'data-src'
			});    

		 jQuery(window).scroll(function(){ // sticky top Menue
			if (jQuery(window).scrollTop() > <?php print ($detect->isMobile()) ? "50" : "80"; ?> ) {
					jQuery('.menuWrapper').addClass('sticky');
			} else {
					jQuery('.menuWrapper').removeClass('sticky');			    
			}		
		});
<?php endif;  ?>

<?php /*CG morphsearch: */
 if($itemid != "856") : ?>
	(function() {
		var bodySite = document.getElementById( 'bodySite' );
		var morphSearch = document.getElementById( 'morphsearch' ),
			input = morphSearch.querySelector( 'input.morphsearch-input'),
			ctrlClose = morphSearch.querySelector( 'span.morphsearch-close' ),
			isOpen = isAnimating = false,
			// show/hide search area
			toggleSearch = function(evt) {
				// return if open and the input gets focused
				if( evt.type.toLowerCase() === 'focus' && isOpen ) return false;

				var offsets = morphsearch.getBoundingClientRect();
				if( isOpen ) {
					classie.remove( morphSearch, 'open' );
					classie.remove( bodySite, 'open' );

					// trick to hide input text once the search overlay closes 
					// todo: hardcoded times, should be done after transition ends
					if( input.value !== '' ) {
						setTimeout(function() {
							classie.add( morphSearch, 'hideInput' );
							setTimeout(function() {
								classie.remove( morphSearch, 'hideInput' );
								input.value = '';
							}, 300 );
						}, 500);
					}
					
					input.blur();
				}
				else {
					classie.add( morphSearch, 'open' );
					classie.add( bodySite, 'open' );
				}
				isOpen = !isOpen;
			};

		// events
		input.addEventListener( 'focus', toggleSearch );
		ctrlClose.addEventListener( 'click', toggleSearch );

		// suggestions löschen nachdem Morphsearch overlay geschlossen wurde weil sonst suggestions position ausserhalb morphsearch - vorläufig beste (einzige) Methode
		jQuery('.morphsearch-close').click('on', function(){
			jQuery('.autocomplete-suggestions').empty();
		});

		// esc key closes search overlay
		// keyboard navigation events
		document.addEventListener( 'keydown', function( ev ) {
			var keyCode = ev.keyCode || ev.which;
			if( keyCode === 27 && isOpen ) {
				toggleSearch(ev);
				jQuery('.autocomplete-suggestions').empty(); //CG siehe kommentar drüber
			}
		} );

	})();
<?php endif; ?>

	/*accordion Menu*/
		var acc = document.getElementsByClassName("subMenuToggle");
		var i;
		for (i = 0; i < acc.length; i++) {
		    jQuery(acc[i]).on("click", function(){
		    	jQuery(this).toggleClass('show');
        		jQuery(this).next('.navSidebar').slideToggle();
        			e.preventDefault();
		    });
		}
		
<?php /*if($itemid == "856") : ?> //CG: suche index seite Search suggestion Situation
		jQuery('.autocomplete-suggestions').detach().appendTo('#search-form');
		jQuery('#searchMore').on("click", function(event){
			event.preventDefault();
			jQuery('input#q').focus();
		});

<?php endif; */ ?>

	});

</script>
</body>

</html>
